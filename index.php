<?php 

	include 'config/setup.php'; 

?>

<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include 'config/css.php'; ?>
	<?php include 'config/js.php'; ?>
</head>

<body>

	<div class="bottom-three">
		<!-- navigation bar begin -->
		<?php include './templates/navbar.php' ?>
		<!-- navigation bar end -->
	</div>

	<!-- body content begin -->
	<div>
		<center><h3>Welcome to CSE IIT Kanpur</h3></center>
	</div>
	<!-- body content end -->

	<!-- footer begin-->
	<?php include './templates/footer.php' ?>
	<!-- footer end -->
</body>
</html>
