<nav class="navbar navbar-inverse .nav-pills navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand " href="#">Dash Board</a>
		</div>

			<!-- navbar will collapse in small screens -->
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
        </button>

		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.php">Home</a></li>
				<li><a href="chart.html">Sample Chart</a></li>
			    
			    <!-- dropdown menu begin-->
				<!-- <li class="dropdown">
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Dropdown <span class="caret"></span></a>
			        <ul class="dropdown-menu">
				        <li><a href="#">A</a></li>
				        <li><a href="#">B</a></li>
				        <li><a href="#">C</a></li> 
				    </ul>
			     </li> -->
			    <!-- drop down menu end -->  
			</ul>

			<!-- sign up and sign in begin -->
			<ul class="nav navbar-nav navbar-right">
		      	<li><a href="register.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
		      	<li> <a href="login.php"> <span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
				<li class="dropdown">
	                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	                    <i class="fa fa-user fa-fw"></i> secondtruth <b class="caret"></b>
	                </a>
	                <ul class="dropdown-menu dropdown-user">
	                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
	                    </li>
	                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
	                    </li>
	                    <li class="divider"></li>
	                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
	                    </li>
	                </ul>
            	</li>
		    </ul>
		    <!-- sign up and sign in end --> 
		</div>
		
		<!-- side bar -->
		<div class="navbar-default sidebar" role="navigation">
		    <div class="sidebar-nav navbar-collapse">

		        <ul class="nav" id="side-menu">
		            <li class="sidebar-search">
		                <div class="input-group custom-search-form">
		                    <input type="text" class="form-control" placeholder="Search...">
		                        <span class="input-group-btn">
		                            <button class="btn btn-primary" type="button">
		                                <i class="fa fa-search"></i>
		                            </button>
		                        </span>
		                </div>
		            </li>
		            <li>
		                <a href="#" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
		            </li>
		            <li>
		                <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
		                <ul class="nav nav-second-level">
		                    <li>
		                        <a href="#">Second Level Item</a>
		                    </li>
		                    <li>
		                        <a href="#">Third Level <span class="fa arrow"></span></a>
		                        <ul class="nav nav-third-level">
		                            <li>
		                                <a href="#">Third Level Item</a>
		                            </li>
		                        </ul>
		                    </li>
		                </ul>
		            </li>
		        </ul>

		    </div>
		</div>
	</div>

</nav>