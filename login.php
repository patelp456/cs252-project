<?php 

	# Start Session:
	session_start();

	# Database Connection:
	include 'config/setup.php'; 

	if($_POST){

		$query = "SELECT * FROM users WHERE email = '$_POST[email]' AND password = SHA1('$_POST[pass]')";
		$result = mysqli_query($dbc, $query);

		if(mysqli_num_rows($result) == 1){
			$_SESSION['username'] = $_POST['email'];
			header('Location: index.php');
		}

	}

	
	$error = false;
	
	if( isset($_POST['btn-login']) ) { 
	 
	 // prevent sql injections/ clear user invalid inputs
	 $email = trim($_POST['email']);
	 $email = strip_tags($email);
	 $email = htmlspecialchars($email);
	 
	 $pass = trim($_POST['pass']);
	 $pass = strip_tags($pass);
	 $pass = htmlspecialchars($pass);
	 // prevent sql injections / clear user invalid inputs
	 
	 if(empty($email)){
	  $error = true;
	  $emailError = "Please enter your email address.";
	 } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
	  $error = true;
	  $emailError = "Please enter valid email address.";
	 }
	 
	 if(empty($pass)){
	  $error = true;
	  $passError = "Please enter your password.";
	 }
	 
	 // if there's no error, continue to login
	 if (!$error) {
	  
	  $password = hash('sha256', $pass); // password hashing using SHA256
	 
	  $res=mysql_query("SELECT userId, userName, userPass FROM users WHERE userEmail='$email'");
	  $row=mysql_fetch_array($res);
	  $count = mysql_num_rows($res); // if uname/pass correct it returns must be 1 row
	  
	  if( $count == 1 && $row['userPass']==$password ) {
	   $_SESSION['user'] = $row['userId'];
	   header("Location: home.php");
	  } else {
	   $errMSG = "Incorrect Credentials, Try again...";
	  }
	   
	 }
	 
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include 'config/css.php'; ?>
	<?php include 'config/js.php'; ?>
</head>

<body>

	<div class="bottom-three">
		<!-- navigation bar begin -->
		<?php include './templates/navbar.php' ?>
		<!-- navigation bar end -->
	</div>

	<!-- body content begin -->
	<div class="container">

		<div id="login-form" class="row">
	    	<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
	    
			    <div class="col-md-6 col-md-offset-3">

					<div class="form-group"> <h2 class="">Sign In.</h2> </div>
					<div class="form-group"> <hr /> </div>

			        <?php
			   			if ( isset($errMSG) ) {
			    	?>

			    	<div class="form-group">
			            <div class="alert alert-danger">
			    			<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
		                </div>
		         	</div>
		            <?php } ?>
			            
					<div class="form-group">
						<label for="inputEmail">Email:</label>
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
							<input type="text" name="email" class="form-control" placeholder="Your Email" value="<?php echo $email; ?>" maxlength="40" />
						</div>
						<span class="text-danger"><?php echo $emailError; ?></span>
					</div>
			            
					<div class="form-group">
						<label for="inputPassword">Password: </label>
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input type="password" name="pass" class="form-control" placeholder="Your Password" maxlength="15" />
						</div>
						<span class="text-danger"><?php echo $passError; ?></span>
					</div>
			            
					<div class="form-group"> <hr /> </div>
			            
					<div class="form-group">
						<button type="submit" class="btn btn-block btn-primary" name="btn-login">Sign In</button>
					</div>

					
			            
					<div class="form-group"> <hr /> </div>

					<div class="form-group">
						<a href="register.php">Sign Up Here...</a>
					</div>

	    		</div>
	   
			</form>
		</div> 
	</div>

	<!-- body content end -->

	<!-- footer begin-->
	<?php include './templates/footer.php' ?>
	<!-- footer end -->
</body>
</html>
