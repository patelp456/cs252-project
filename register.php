<?php 

	include 'config/setup.php'; 

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include 'config/css.php'; ?>
	<?php include 'config/js.php'; ?>
</head>

<body>

	<div class="bottom-three">
		<!-- navigation bar begin -->
		<?php include './templates/navbar.php' ?>
		<!-- navigation bar end -->
	</div>


	<!-- body content begin -->
	<div id="login-form">
	
<<<<<<< HEAD
	
=======
	<?php 
		// if(isset($_POST['submitted'])==1){

		// 	$query="INSERT INTO users (name,email,pass) VALUES ('$_POST[name]','$_POST[email]','$_POST[pass]')"	;
		// 	$result= mysqli_query($dbc,$query);
			
		// 	if($result){

		// 		echo '<p>User was added!</p>';
		// 	}else{
		// 		echo '<p>User could not be added because:' .mysqli_error($dbc);
		// 		echo '<p>'.$query.'</p>'
		// 	}


		// }

	?>	
>>>>>>> 86f36fe0bb90d2cda53103580e964d53738c4426
		<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">

			<div class="col-md-5 col-md-offset-3">

				<div class="form-group"> <h2 class="">Sign Up.</h2> </div>		
				<div class="form-group"><hr /> </div>

			<?php
				if ( isset($errMSG) ) {
			?>

				<div class="form-group">
					<div class="alert alert-<?php echo ($errTyp=="success") ? "success" : $errTyp; ?>">
						<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
					</div>
				</div>

			<?php } ?>

				<div class="form-group">
					<label for="inputName">Name</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						
							<input type="text" id="inputName" name="name" class="form-control" placeholder="Enter Name" maxlength="50" value="<?php echo $name ?>" />	
					</div>
					<span class="text-danger"><?php echo $nameError; ?></span>
				</div>

				<div class="form-group">
					<label for="inputEmail">Email</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
						
							<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Enter Your Email" maxlength="40" value="<?php echo $email ?>" />
					</div>
				<span class="text-danger"><?php echo $emailError; ?></span>
				</div>

				<div class="form-group">
					<label for="inputDepartment">Department</label>
					<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-education"></span></span>
						
						<input type="text" id="inputDepartment" name="department" class="form-control" placeholder="Enter Department" maxlength="50" value="<?php echo $department ?>"/>
					</div>
					<span class="text-danger"><?php echo $nameError; ?></span>
				</div>

				<div class="form-group">
					<label for="inputProgram">Program</label>
					<div class="input-group">
					
						<span class="input-group-addon"><span class="glyphicon glyphicon-education"></span></span>
							
							<input type="text" name="program" id="inputProgram" class="form-control" placeholder="b.tech" maxlength="50" value="<?php echo $program ?>" />
					</div>
					<span class="text-danger"><?php echo $nameError; ?></span>
				</div>

				<div class="form-group">
				<label for="inputBatch">Batch</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-education"></span></span>
							
							<input type="text" name="batch" id="inputBatch" class="form-control" placeholder="Batch" maxlength="50" value="<?php echo $batch ?>" />
					</div>
					<span class="text-danger"><?php echo $nameError; ?></span>
				</div>

				<div class="form-group">
				<label for="inputAddress">Home Address</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-home"></span></span>
							
							<input type="text" name="address" id="inputAddress" class="form-control" placeholder="City" maxlength="50" value="<?php echo $address ?>" />
					</div>
					<span class="text-danger"><?php echo $nameError; ?></span>
				</div>

				<div class="form-group">
				<label for="inputNumber">Phone Number</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
					       
							<input type="text" name="phone" id="inputNumber" class="form-control" placeholder="Phone Number" maxlength="50" value="<?php echo $phone ?>" />
					</div>
					<span class="text-danger"><?php echo $nameError; ?></span>
				</div>

				<div class="form-group">
				<label for="inputPassword">Password</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					    
							<input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Enter Password" maxlength="15" />
					</div>
					<span class="text-danger"><?php echo $passError; ?></span>
				</div>

				<div class="form-group"><hr /> </div>

				<div class="form-group">
					<button type="submit" class="btn btn-block btn-primary" name="btn-signup">Sign Up</button>
				</div>

				<div class="form-group"><hr /> </div>

				<div class="form-group">
					<a href="login.php">Sign in Here...</a>
				</div>

				<input type="hidden" name="submitted" value="1">

			</div>

		</form>
	</div> 
	<!-- body content end -->

	<!-- footer begin-->
	<?php include './templates/footer.php' ?>
	<!-- footer end -->
</body>
</html>
