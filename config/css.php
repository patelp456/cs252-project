<?php

// css files here

?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- Jquery CSS -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"/>

<!-- Fontawesome -->
<script src="https://use.fontawesome.com/0190b5eeb3.js"></script>
		
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">


<style>

	.bottom-three {
	   margin-bottom: 3.0cm;
	}

	/* Sticky footer styles
	-------------------------------------------------- */
	html {
	  position: relative;
	  min-height: 100%;
	}
	body {
	  /* Margin bottom by footer height */
	  margin-bottom: 60px;
	}
	.footer {
	  position: absolute;
	  bottom: 0;
	  width: 100%;
	  /* Set the fixed height of the footer here */
	  height: 60px;
	  background-color: #181716;
	}

	/* Custom page CSS
	-------------------------------------------------- */
	/* Not required for template or sticky footer method. */

	body > .container {
	  padding: 60px 15px 0;
	}
	.container .text-muted {
	  margin: 20px 0;
	}

	.footer > .container {
	  padding-right: 15px;
	  padding-left: 15px;
	}

	code {
	  font-size: 80%;
	}


	body {
	    background-color: #f8f8f8;
	}

	#wrapper {
	    width: 100%;
	}

	#page-wrapper {
	    padding: 30px 15px 0 15px;
	    min-height: 568px;
	    background-color: #fff;
	}

	@media(min-width:768px) {
	    #page-wrapper {
	        position: inherit;
	        margin-left: 250px;
	        padding: 30px 30px 0 30px;
	        border-left: 1px solid #e7e7e7;
	    }
	}

	.navbar-header {
	    float: left;
	}

	.navbar-left {
	    float: left;
	}

	.navbar-right {
	    text-align: right;
	}

	.navbar-top-links {
	    margin: 0;
	}

	.navbar-top-links li {
	    display: inline-block;
	}

	.navbar-top-links li:last-child {
	    margin-right: 6px;
	}

	.navbar-top-links li a {
	    padding: 15px;
	    min-height: 50px;
	}

	.navbar-top-links>li>a {
	    color: #999;
	}

	.navbar-top-links>li>a:hover, .navbar-top-links>li>a:focus, .navbar-top-links>.open>a, .navbar-top-links>.open>a:hover, .navbar-top-links>.open>a:focus {
	    color: #fff;
	    background-color: #222;
	}

	.navbar-top-links .dropdown-menu li {
	    display: block;
	}

	.navbar-top-links .dropdown-menu li:last-child {
	    margin-right: 0;
	}

	.navbar-top-links .dropdown-menu li a {
	    padding: 3px 20px;
	    min-height: 0;
	}

	.navbar-top-links .dropdown-menu li a div {
	    white-space: normal;
	}

	.navbar-top-links .dropdown-messages,
	.navbar-top-links .dropdown-tasks,
	.navbar-top-links .dropdown-alerts {
	    width: 310px;
	    min-width: 0;
	}

	.navbar-top-links .dropdown-messages {
	    margin-left: 5px;
	}

	.navbar-top-links .dropdown-tasks {
	    margin-left: -59px;
	}

	.navbar-top-links .dropdown-alerts {
	    margin-left: -123px;
	}

	.navbar-top-links .dropdown-user {
	    right: 0;
	    left: auto;
	}

	.sidebar .sidebar-nav.navbar-collapse {
	    padding-right: 0;
	    padding-left: 0;
	}

	.sidebar .sidebar-search {
	    padding: 15px;
	}

	.sidebar ul li {
	    border-bottom: 1px solid #e7e7e7;
	}

	.sidebar ul li a.active {
	    background-color: #eee;
	}

	.sidebar .arrow {
	    float: right;
	}

	.sidebar .fa.arrow:before {
	    content: "\f104";
	}

	.sidebar .active>a>.fa.arrow:before {
	    content: "\f107";
	}

	.sidebar .nav-second-level li,
	.sidebar .nav-third-level li {
	    border-bottom: 0!important;
	}

	.sidebar .nav-second-level li a {
	    padding-left: 37px;
	}

	.sidebar .nav-third-level li a {
	    padding-left: 52px;
	}

	@media(min-width:768px) {
    .sidebar {
        z-index: 1;
        position: absolute;
        width: 250px;
        margin-top: 51px;
    }

	    .navbar-top-links .dropdown-messages,
	    .navbar-top-links .dropdown-tasks,
	    .navbar-top-links .dropdown-alerts {
	        margin-left: auto;
	    }
	}

	

</style>