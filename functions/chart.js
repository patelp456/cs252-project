function generateChart (cse,msc,ee,bsbe,me) {
	var chart = c3.generate({
		bindto: document.getElementById('dvImportSegments'),
	    data: {
    	    columns: [
		    	['CSE',cse],
		    	['MSC',msc],
		    	['EE',ee],
		    	['BSBE',bsbe],
		    	['ME',me],
 		    ],
		    type: 'bar'
	    }
	});
};

function ChartHandler (data) {

	var cse = 0;
	var msc = 0;
	var ee = 0;
	var bsbe = 0;
	var me  = 0;	
	for (var i = 1; i < data.length; i++) {

		if (data[i][1] == 'CSE') 
		{
			cse++;
		} 
		else if (data[i][1] == 'MSC') 
		{
			msc++;
		} 
		else if (data[i][1] == 'EE') 
		{
			ee++;
		} 
		else if (data[i][1] == 'BSBE') 
		{
			bsbe++;
		} 
		else if (data[i][1] == 'ME') 
		{
			me++;
		}	
	}
	generateChart (cse,msc,ee,bsbe,me);
};
// Method that reads and processes the selected file
$(document).ready(function() {

	// The event listener for the file upload
	document.getElementById('txtFileUpload').addEventListener('change', upload, false);

	// Method that checks that the browser supports the HTML5 File API
	function browserSupportFileUpload() 
	{
	    var isCompatible = false;
	    
	    if (window.File && window.FileReader && window.FileList && window.Blob) 
	    {
	    	isCompatible = true;
	    }

	    return isCompatible;
	}

	// Method that reads and processes the selected file
	function upload(evt) {
	    if (!browserSupportFileUpload()) 
	    {
	        alert('The File APIs are not fully supported in this browser!');
	    } 
	    else 
	    {
            var data = null;
            var file = evt.target.files[0];
            var reader = new FileReader();
            reader.readAsText(file);

            reader.onload = function(event) 
            {
                var csvData = event.target.result;
                data = $.csv.toArrays(csvData);

                if (data && data.length > 0) 
                {
                    //alert('Imported -' + data.length + '- rows successfully!');
                    //document.write(data);
                    ChartHandler(data);
                } 
                else 
                {
                    alert('No data to import!');
                }
            };

            reader.onerror = function() 
            {
                alert('Unable to read ' + file.fileName);
            };
        }
	}
});